from recon.core.module import BaseModule
from recon.mixins.resolver import ResolverMixin
import dns.resolver
import os
from multiprocessing import Pool
import socket
from recon.mixins.threads import ThreadingMixin
from multiprocessing import Pool
import tld
import dns.resolver
import re

wildcard = None

def dns_resolve(host):
	domain = host
	resolver = dns.resolver

	global wildcard


	max_attempts = 3
	attempt = 0
	while attempt < max_attempts:
		try:
			answers = resolver.query(domain)
		except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
			return None
		except dns.resolver.Timeout:
			#pass#self.verbose('%s => Request timed out.' % (host))
			attempt += 1
			continue
		except resolver.NoNameservers:
			attempt+=1
			continue
		except Exception as e:
			print str(e)
			return None

		#print "here!!"
		if wildcard:
			for w in wildcard:
				if answers.response.answer[0][0] == w[0]:
					return None
		#if answers.response.answer[0][0] == wildcard:
		#	attempt += max_attempts
		#else:
		output = []
		# process answers
		if answers.response.answer[0][0] != wildcard:
			for answer in answers.response.answer:
				for rdata in answer:
					if rdata.rdtype in (1, 5):
						if rdata.rdtype == 1:
							address = rdata.address
							print('%s => (A) %s - Host found!' % (domain, address))
							output.append(('A', domain,address))
							#self.add_hosts(host, address, module_name="brute_hosts")
						if rdata.rdtype == 5:
							cname = rdata.target.to_text()[:-1]
							print('%s => (CNAME) %s - Host found!' % (domain, cname))
							output.append(('CNAME', domain,cname))
							#self.add_hosts(cname)
							# add the host in case a CNAME exists without an A record
							#self.add_hosts(host, module_name="brute_hosts")
			return output




def check_url(url):
	try:
		#print("Trying ....", url)
		res = socket.gethostbyname(url)
		if res:
			print("Found %s"%(url))
			#self.add_hosts(url)
			return url
	except Exception as e:

		pass

class Module(BaseModule, ResolverMixin,ThreadingMixin):
	meta =  {
			'name':'Prefix scanner for subdomains',
			'author':'Ibrahim M. El-Sayed (the_storm)-(@ibarhim-mosaad)',
			'description': 'Bruteforces the prefix of subdomains such as corp.test.com ==> corp-dev.test.com',
			'Comments':('No comments I have in my mind so far'),
			'query': 'SELECT DISTINCT domain FROM domains WHERE domain IS NOT NULL',
			'options': (
			('prefix', os.path.join(BaseModule.data_path, 'subdomains_prefix.txt'), True, 'path to public prefix wordlist'),
			('file', None, False, "Input file"),
			('insert', None, False, "insert into the db??"),
			#('file', "", ''), True, 'file to read data from'),
			
		),
	}

	def module_run(self, subdomain):
		#I will only split on the first half dot and that's it 
		global wildcard
		f = open(self.options['prefix'], 'r')
		prefixes = f.read().splitlines()
		f.close()
		domains = []
		permutated_domains = []
		if self.options['file']:
			f = open(self.options['file'], 'r')
			subdomain = f.read().splitlines()
			f.close()

		for d in subdomain:
			if "http://" != d[0:7] and "https://" != d[0:8]:
				temp_tld = tld.get_tld("http://"+d, fail_silently=True)
			else:
				temp_tld = tld.get_tld(d, fail_silently=True)
			#self.output(d)
			if d != temp_tld: #make sure it is not TLD. I am not interested in the TLD ... only subdomains ..
				wildcard = None
				try:
					temp_d = d.split(".",1)[1]
					answers = dns.resolver.query('*.%s' % (temp_d))
					wildcard = answers.response.answer #answers.response.answer[0][0]
					self.output('Wildcard DNS entry found for \'%s\' at \'%s\'.' % (d, wildcard))
				except (dns.resolver.NoNameservers, dns.resolver.Timeout):
					self.error('Invalid nameserver.')
					return
				except (dns.resolver.NXDOMAIN, dns.resolver.NoAnswer):
					self.verbose('No Wildcard DNS entry found.')

				for prefix in prefixes:
					permutated_domain = (prefix+".").join(d.split('.',1))
					#self.verbose("subdomain: %s" %(permutated_domain))
					permutated_domains.append(permutated_domain)

		found = []
		no_processes = min(8, len(permutated_domains))
		p = Pool(no_processes)
		res = p.map(dns_resolve, permutated_domains)
		p.close()
		p.join()
		for val in res:
			if val:
				for resp in val:
					if resp[0] == "A" and self.options['insert']:
						#self.output("A ==> %s" %(resp[1]))
						self.add_hosts(resp[1], ip_address=resp[2], module_name="bruteforce prefix")
					elif resp[0] == "CNAME" and self.options['insert']:
						#self.output("CNAME ==> %s" %(resp[2]))
						self.add_hosts(resp[1], module_name="bruteforce prefix")
						self.add_hosts(resp[2], module_name="bruteforce prefix")



		return res

		#self.thread(permutated_domains)
		#To-Do add the found result in a list .... from the threading ...just add the lock!

	def module_thread(self, permutated_domain):
		try:
			self.output(permutated_domains)
			res = socket.gethostbyname(permutated_domain)
			if res:
				self.output("Found %s"%(permutated_domain))
				self.add_hosts(permutated_domain)
				#found.append(permutated_domain)
		except:
			pass