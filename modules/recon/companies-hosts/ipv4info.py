from recon.core.module import BaseModule
from recon.mixins.resolver import ResolverMixin
import dns.resolver
import os
from multiprocessing import Pool
import socket
from recon.mixins.threads import ThreadingMixin
from multiprocessing import Pool
import tld
import dns.resolver
import re
from bs4 import BeautifulSoup
import netaddr


class Module(BaseModule, ResolverMixin,ThreadingMixin):
	meta =  {
			'name':'IPv4info free text DNS search',
			'author':'Ibrahim M. El-Sayed (the_storm)-(@ibarhim-mosaad)',
			'description': 'This module uses the free text search capability of ipv4info to retrieve hosts about a specific company',
			'Comments':(''),
			'query': 'SELECT DISTINCT company FROM companies WHERE company IS NOT NULL'
	}

	def module_run(self, companies):
		url = "http://ipv4info.com/?act=check&ip=%s&x=6&y=7"
		output = []
		for c in companies:
			u = url%(c)
			resp = self.request(u)
			#<tr\s+>.*?<a href="/block-info/(.*?)\.html">?.+?<a\s+href="/as-info/.*?></td>\s*<td\s*>(.*?)</td><td\s*><a\s+href=.*?>(.*?)</a>.*?</tr>
			#res = re.findall('<tr\s+>.*?<a href="/block-info/(.*?)\.html">?.+?<a\s+?href="/as-info/.+?\.html">.+?</a>\s*</td>\s*<td\s*>\s*(<a href="/subblocks/.*?.html">)?(.+?)(</a>)?</td>\s*<td\s*><a\s+href=.*?>(.*?)</a>.*?</tr>',resp.text, flags=re.DOTALL)
			soup = BeautifulSoup(resp.text, 'html.parser')
			table = soup.find('table', attrs={'class':'TB2'})
			rows = table.find_all("tr")
			for row in rows[3:]:
				cols = row.find_all("td")
				start_ip = netaddr.IPAddress(cols[2].text)
				end_ip = netaddr.IPAddress(cols[3].text)
				block_name = cols[7].text
				company_name = cols[8].text	
				if "<a href=" in block_name:
					block_name = re.findall('<a href=".*?">(.*?)</a>\s*</td>', block_name)

				if "<a href=" in company_name:
					company_name = re.findall('<a href=".*?">(.*?)</a>\s*</td>', company_name)

				if c.lower() in company_name.lower():
					self.output("%s-%s\tblock name:%s\tCompany: %s" %(str(start_ip), str(end_ip), block_name, company_name))

					while start_ip <= end_ip:
						self.add_hosts(ip_address=str(start_ip), mute=True)
						start_ip+=1


